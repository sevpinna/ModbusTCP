package ModbusTCP

import (
	"fmt"
	"testing"
	"time"
)

var x uint16

func TestXx(t *testing.T) {
	//x:=`{"IP":"127.0.0.1","ID":1,"Register":"0x03","Addr":0,"Swap":false,"Bit":0}`
	//AddTag_test(1,"bool",`{"IP":"127.0.0.1:502","ID":1,"Register":"0x01","Addr":0,"Swap":false,"Bit":0}`)
	//AddTag_test(2,"bool",`{"IP":"127.0.0.1:502","ID":1,"Register":"0x02","Addr":0,"Swap":false,"Bit":0}`)
	_ = AddTag(3, "uint16", `{"IP":"127.0.0.1:502","ID":1,"Register":"0x03","Addr":0,"Swap":true,"Bit":0}`)
	//AddTag_test(4,"uint16",`{"IP":"127.0.0.1:502","ID":1,"Register":"0x04","Addr":0,"Swap":false,"Bit":-1}`)
	Start(xx)
	c := time.Tick(5 * time.Second)
	for {
		<-c
		Write(3, uint16(1))
	}
}
func xx(id int, v interface{}, time int64) {
	fmt.Println(id, v, time)
	//x = v.(uint16)
}
