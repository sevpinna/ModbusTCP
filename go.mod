module gitee.com/sevpinna/ModbusTCP

go 1.15

require (
	gitee.com/sevpinna/Aweto v0.0.0-20201108041742-4385d7640098
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.7.0
)
